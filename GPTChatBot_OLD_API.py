# This bot has been turned off due to the OpenAI API not being free

import openai
import os
import json
from telegram import __version__ as TG_VER

try:
    from telegram import __version_info__
except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]

if __version_info__ < (20, 0, 0, "alpha", 5):
    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )

from telegram import Update
from telegram.ext import filters, MessageHandler, ApplicationBuilder, CommandHandler, ContextTypes, ConversationHandler


OPEN_AI_KEY = os.getenv("OPENAI_API_KEY")
BOT_TOKEN = os.getenv("CHAT_BOT_TOKEN")

openai.api_key = OPEN_AI_KEY

ECHOING = range(1)
convos  = dict()

################################################################################
# OpenAI

# GPT Models:
GPT3_5_TURBO = "gpt-3.5-turbo"
GPT4_PI = "gpt-4-0314"
GPT4_0613 = "gpt-4-0613"
GPT4_32K = 'gpt-4-32k'
# Default GPT model:
GPT4 = "gpt-4"

model = GPT4
MAX_LENGTH = 4096


def deleteHistory(chatId):
    global convos
    convos['{}'.format(chatId)].clear()


def generatePrompt(msg, chatId):
    global convos
    promptMsg = {"role": "user", "content": msg}
    convos['{}'.format(chatId)] = convos.get('{}'.format(chatId), []) + [promptMsg]


def generateReply(messageHistory, chatId, gptModel = GPT4, temp = 0):
    global convos
    responseMsg = None

    try:
        response = openai.ChatCompletion.create(
            model = gptModel,
            messages = messageHistory,
            temperature = temp,
        )
        responseMsg = {"role": "assistant", "content": response['choices'][0]['message']['content']}
    except Exception as e:
        return '{}\n\nERROR: GPT failed to reply, please try again later...'.format(e)
    
    convos['{}'.format(chatId)] = convos.get('{}'.format(chatId), []) + [responseMsg]

    return response['choices'][0]['message']['content']
    

#################################################################################################


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    global convos
    chatId = update.effective_chat.id

    if '{}'.format(chatId) not in convos:
        convos['{}'.format(chatId)] = list()

    try:
        await context.bot.send_message(chat_id=chatId, text="Send /deleteHistory to erase the bot's memory\n\nSend /info to learn more about this bot\n\nSend /stop to stop the conversation\n\nTo start the conversation, simply send a message!")
        await context.bot.send_message(chat_id=update.effective_chat.id, text="Waiting for your message...")
    except Exception as e:
        print("User blocked the bot due to no response after sending /start")

    return ECHOING


async def delHistory(update: Update, context: ContextTypes.DEFAULT_TYPE):
    global convos
    chatId = update.effective_chat.id
    deleteHistory(chatId)
    await context.bot.send_message(chat_id=update.effective_chat.id, text="Sucesfully deleted the bot's memory.")


async def exitConvo(update: Update, context: ContextTypes.DEFAULT_TYPE):
    global convos
    await context.bot.send_message(chat_id=update.effective_chat.id, text='Succesfully exited conversation.\n\nTo delete conversation history, send /deleteHistory\nTo continue conversation, send another message\n\n')
    # Not used in this implementation, but for future coding this might be useful:
    return ConversationHandler.END


async def echoCONTACT(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="This bot is unable to reply to contacts.")


async def echoPHOTO(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="This bot is unable to reply to pictures.")


async def echo(update: Update, context: ContextTypes.DEFAULT_TYPE):
    global convos

    message = update.message.text

    chatId = update.effective_chat.id

    if '{}'.format(chatId) not in convos:
        convos['{}'.format(chatId)] = list()

    if message.lower() == 'exit' or message.lower() == 'stop' or message.lower() == 'cancel':
        await context.bot.send_message(chat_id=update.effective_chat.id, text="Succesfully exited conversation.\n\nSend /deleteHistory to delete the bot's memory\n\nSend /start to start a new conversation\n")
        return ConversationHandler.END

    generatePrompt(message, chatId)

    reply = generateReply(convos['{}'.format(chatId)], chatId)

    with open('convos.json', 'w', encoding="utf-8") as file:
        file.write(json.dumps(convos, indent = 4, ensure_ascii = False))  

    if len(reply) > MAX_LENGTH:
        await context.bot.send_message(chat_id=update.effective_chat.id, text="Unexpected error, GPT's response was too long, we are currently trying to fix this issue...")
        return

    await context.bot.send_message(chat_id=update.effective_chat.id, text=reply)
    return ECHOING


async def info(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    global convos
    await context.bot.send_message(chat_id=update.effective_chat.id, text="GPT Model:\n{}\n\nAvailable Models:\ngpt-3.5-turbo\ngpt-4-0314\ngpt-4-0613\ngpt-4\n\n**This bot is not associated in any way with OpenAI, it simply works through its API**\n\n**This bot is still under development**".format(model))
    return ECHOING


async def unknown(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="Sorry, I didn't understand that command.")


if __name__ == '__main__':
    try:
        with open('convos.json', 'r', encoding='utf-8') as file:
            convos = json.load(file)
    except:
        convos = dict()
    
    application = ApplicationBuilder().token(BOT_TOKEN).build()
    
    start_handler = CommandHandler('start', start)
    history_handler = CommandHandler('deleteHistory', delHistory)
    exit_handler1 = CommandHandler('stop', exitConvo)
    exit_handler2 = CommandHandler('exit', exitConvo)
    exit_handler3 = CommandHandler('cancel', exitConvo)
    echo_handler = MessageHandler(filters.TEXT & (~filters.COMMAND), echo)
    echoPHOTO_handler = MessageHandler(filters.PHOTO, echoPHOTO)
    echoCONTACT_handler = MessageHandler(filters.CONTACT, echoCONTACT)
    info_handler = CommandHandler('info', info)
    unknown_handler = MessageHandler(filters.COMMAND, unknown)
    
    application.add_handler(start_handler)
    application.add_handler(history_handler)
    application.add_handler(exit_handler1)
    application.add_handler(exit_handler2)
    application.add_handler(exit_handler3)
    application.add_handler(echo_handler)
    application.add_handler(echoPHOTO_handler)
    application.add_handler(echoCONTACT_handler)
    application.add_handler(info_handler)
    # unknown handler should be at the end!!!
    application.add_handler(unknown_handler)
    
    application.run_polling()
