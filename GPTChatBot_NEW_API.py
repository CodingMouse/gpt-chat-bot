# This bot has been turned off due to the OpenAI API not being free

from openai import OpenAI

# Note: There's no need to provide the OPENAI API key anymore, but you still need your
# key in an environmental variable called 'OPENAI_API_KEY'
client = OpenAI()

import os
import json

from telegram import __version__ as TG_VER

try:
    from telegram import __version_info__
except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]

if __version_info__ < (20, 0, 0, "alpha", 5):
    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )


from telegram import Update
from telegram.ext import filters, MessageHandler, ApplicationBuilder, CommandHandler, ContextTypes, ConversationHandler


BOT_TOKEN = os.getenv("CHAT_BOT_TOKEN")
convos  = dict()

################################################################################
# OpenAI

# GPT Models:
GPT3_5_TURBO = "gpt-3.5-turbo"
GPT4_PI = "gpt-4-0314"
GPT4_0613 = "gpt-4-0613"
GPT4_32K = 'gpt-4-32k'
GPT4_TURBO = 'gpt-4-1106-preview'
GPT4_VISION = 'gpt-4-vision-preview'
GPT4_0125 = 'gpt-4-0125-preview'
GPT4 = "gpt-4"

models = frozenset({GPT3_5_TURBO, GPT4_PI, GPT4_0613, GPT4_32K, GPT4_TURBO, GPT4_VISION, GPT4, GPT4_0125})

MAIN_MODEL = GPT4_TURBO
MAX_LENGTH = 4096


def deleteHistory(chatId):
    global convos
    convos['{}'.format(chatId)].clear()


def generatePrompt(msg, chatId):
    global convos
    promptMsg = {"role": "user", "content": msg}
    convos['{}'.format(chatId)] = convos.get('{}'.format(chatId), []) + [promptMsg]


def generateReply(messageHistory, chatId, gptModel = MAIN_MODEL, temp = 0.0):
    global convos

    responseMsg = None
    try:
        response = client.chat.completions.create(model = gptModel,
        messages = messageHistory,
        temperature = temp)

        # OLD OPENAI API VERSION:
        # responseMsg = {"role": "assistant", "content": response['choices'][0]['message']['content']}

        # UPDATED VERSION:
        replyContent = response.choices[0].message.content
        responseMsg = {"role": "assistant", "content": replyContent}

    except Exception as e:
        print("\n > ERROR:\n")
        print(" > {}".format(e))
        return 'ERROR: GPT failed to reply, please try again later...'
    
    convos['{}'.format(chatId)] = convos.get('{}'.format(chatId), []) + [responseMsg]

    # OLD OPENAI API VERSION:
    # return response['choices'][0]['message']['content']
    
    # UPDATED VERSION:
    return replyContent


def generateVisionReply(url):
    response = client.chat.completions.create(
        model="gpt-4-vision-preview",
        messages=[
            {
                "role": "user",
                "content": [
                    {
                        "type": "image_url",
                        "image_url": url,
                    },
                    {
                        "type": "text",
                        "text": "If the image shows an unanswered question or problem, provide only the final answer. If it's something else, provide a short description.",
                    },
                ],
            }
        ],
        max_tokens=300,
    )
    responseContent = response.choices[0].message.content
    return responseContent
    

#################################################################################################


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    global convos
    
    chatId = update.effective_chat.id

    if '{}'.format(chatId) not in convos:
        convos['{}'.format(chatId)] = list()

    try:
        await context.bot.send_message(chat_id=chatId, text="Send /deleteHistory to erase the bot's memory\n\nSend /info to learn more about this bot\n\nSend /stop to stop the conversation\n\nTo start the conversation, simply send a message!")
        await context.bot.send_message(chat_id=update.effective_chat.id, text="Waiting for your message...")
    except Exception as e:
        print("\n > ERROR:\n")
        print(" > {}".format(e))
        print("User probably blocked the bot due to no response after sending /start")


async def delHistory(update: Update, context: ContextTypes.DEFAULT_TYPE):
    global convos
    chatId = update.effective_chat.id
    deleteHistory(chatId)
    await context.bot.send_message(chat_id=update.effective_chat.id, text="> Sucesfully deleted the bot's memory.")


async def exitConvo(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text='> Succesfully exited conversation.\n\n> To delete conversation history, send /deleteHistory\n> To continue conversation, send another message\n\n')
    return ConversationHandler.END


async def echoDOC(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="This bot is unable to reply to documents.")


async def echoCONTACT(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="This bot is unable to reply to contacts.")


# NEW API NOW SUPPORTS IMAGE INPUTS:
async def echoPHOTO(update: Update, context: ContextTypes.DEFAULT_TYPE):
    file_id = update.message.photo[-1].file_id  # photo field is a list, and the highest resolution photo is usually the last element
    file_info = await context.bot.get_file(file_id)
    photo_url = f'{file_info.file_path}'

    await context.bot.send_message(chat_id=update.effective_chat.id, text="> Generating reply, please wait...")

    visionReply = generateVisionReply(photo_url)

    await context.bot.send_message(chat_id=update.effective_chat.id, text=visionReply)


async def echo(update: Update, context: ContextTypes.DEFAULT_TYPE):
    global convos

    message = update.message.text
    chatId = update.effective_chat.id

    if '{}'.format(chatId) not in convos:
        convos['{}'.format(chatId)] = list()

    if message.lower() == 'exit' or message.lower() == 'stop' or message.lower() == 'cancel':
        await context.bot.send_message(chat_id=update.effective_chat.id, text="> Succesfully exited conversation.\n\nSend /deleteHistory to delete the bot's memory\n\nSend /start to start a new conversation\n")
        return ConversationHandler.END

    generatePrompt(message, chatId)

    reply = generateReply(convos['{}'.format(chatId)], chatId, MAIN_MODEL, 1)

    with open('convos.json', 'w', encoding="utf-8") as file:
        file.write(json.dumps(convos, indent = 4, ensure_ascii = False))  

    if len(reply) > MAX_LENGTH:
        await context.bot.send_message(chat_id=update.effective_chat.id, text="UNEXPECTED ERROR: GPT's response is too large!")
        return

    await context.bot.send_message(chat_id=update.effective_chat.id, text=reply)


async def info(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    global convos
    chatId = update.effective_chat.id
    infoMessage = "GPT Model:\n{}\n\nAvailable Models:".format(MAIN_MODEL)

    availableModels = ""
    for model in models:
        availableModels += "\n{}".format(model)
        
    infoMessage += availableModels + "\n\n**This bot is not associated in any way with OpenAI, it simply works through its API**\n\n**This bot is still under development**"

    await context.bot.send_message(chat_id=chatId, text=infoMessage)


async def unknown(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="Sorry, I didn't understand that command.")


if __name__ == '__main__':
    try:
        with open('convos.json', 'r', encoding='utf-8') as file:
            convos = json.load(file)
    except:
        convos = dict()
    
    application = ApplicationBuilder().token(BOT_TOKEN).build()
    
    start_handler = CommandHandler('start', start)
    history_handler1 = CommandHandler('deleteHistory', delHistory)
    history_handler2 = CommandHandler('clear', delHistory)
    history_handler3 = CommandHandler('cls', delHistory)
    exit_handler1 = CommandHandler('stop', exitConvo)
    exit_handler2 = CommandHandler('exit', exitConvo)
    exit_handler3 = CommandHandler('cancel', exitConvo)
    echo_handler = MessageHandler(filters.TEXT & (~filters.COMMAND), echo)
    echoPHOTO_handler = MessageHandler(filters.PHOTO, echoPHOTO)
    echoCONTACT_handler = MessageHandler(filters.CONTACT, echoCONTACT)
    echoDOCUMENT_handler = MessageHandler(filters.ATTACHMENT, echoDOC)
    info_handler = CommandHandler('info', info)
    unknown_handler = MessageHandler(filters.COMMAND, unknown)
    
    application.add_handler(start_handler)
    application.add_handler(history_handler1)
    application.add_handler(history_handler2)
    application.add_handler(history_handler3)
    application.add_handler(exit_handler1)
    application.add_handler(exit_handler2)
    application.add_handler(exit_handler3)
    application.add_handler(echo_handler)
    application.add_handler(echoPHOTO_handler)
    application.add_handler(echoCONTACT_handler)
    application.add_handler(echoDOCUMENT_handler)
    application.add_handler(info_handler)
    # unknown handler should be at the end!!!
    application.add_handler(unknown_handler)
    
    try:
        application.run_polling()
    except Exception as e:
        print("\n > ERROR:\n")
        print(" > {}".format(e))
